package com.brunofelipe.buyandsell.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.brunofelipe.buyandsell.entity.Categorys;
import com.brunofelipe.buyandsell.repositories.CategorysRepository;
import com.brunofelipe.buyandsell.services.exception.DataIntegrityException;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@Service
public class CategorysService {
	
	@Autowired
	private CategorysRepository categorysRepository;
	
	
	public Categorys search(Integer idCategorys) throws ObjectNotFoundException {
		Optional<Categorys> obj = categorysRepository.findById(idCategorys);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
					"Objeto não foi encontrado pelo seu Id "+ idCategorys +", tipo: "+ Categorys.class.getName()));
	}
	
	public Categorys insert(Categorys categorys) {
		categorys.setIdCategorys(null);
		return categorysRepository.save(categorys);
	}
	
	public Categorys update(Categorys categorys) {
		return categorysRepository.save(categorys);
	}

	public void delete(Integer idCategorys) {
		search(idCategorys);
		try {
			categorysRepository.deleteById(idCategorys);
		}
		catch (DataIntegrityViolationException die) {
			throw new DataIntegrityException("Não é possivel deletar pois existe uma FK na mesma.");
		}
	}
	
	public List<Categorys> findAll() {
		return categorysRepository.findAll();
	}
	
}
