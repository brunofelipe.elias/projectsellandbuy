package com.brunofelipe.buyandsell.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brunofelipe.buyandsell.entity.Requests;
import com.brunofelipe.buyandsell.repositories.RequestsRepository;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;

@Service
public class RequestsService {

	@Autowired
	private RequestsRepository requestsRepository;

	public Requests search(Integer idRequests) throws ObjectNotFoundException {
		Optional<Requests> obj = requestsRepository.findById(idRequests);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não foi encontrado pelo seu Id " + idRequests + ", tipo: " + Requests.class.getName()));
	}
}