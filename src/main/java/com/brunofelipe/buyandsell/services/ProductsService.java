package com.brunofelipe.buyandsell.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brunofelipe.buyandsell.entity.Products;
import com.brunofelipe.buyandsell.repositories.ProductsRepository;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@Service
public class ProductsService {
	
	@Autowired
	private ProductsRepository productsRepository;

	
	public Products search(Integer idProducts) throws ObjectNotFoundException {
		Optional<Products> obj = productsRepository.findById(idProducts);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
					"Objeto não foi encontrado pelo seu Id "+ idProducts +", tipo: "+ Products.class.getName()));
	}
}