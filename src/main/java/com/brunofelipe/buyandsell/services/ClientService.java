package com.brunofelipe.buyandsell.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brunofelipe.buyandsell.entity.Client;
import com.brunofelipe.buyandsell.repositories.ClientRepository;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository; 
	
	public Client search(Integer idClient) throws ObjectNotFoundException {
		Optional<Client> obj = clientRepository.findById(idClient);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
					"Objeto não foi encontrado pelo seu Id "+ idClient +", tipo: "+ Client.class.getName()));
	}

}
