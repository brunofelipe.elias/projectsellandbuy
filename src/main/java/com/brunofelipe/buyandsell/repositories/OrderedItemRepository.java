package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunofelipe.buyandsell.entity.OrderedItem;

@Repository
public interface OrderedItemRepository extends JpaRepository <OrderedItem, Integer> {

}
