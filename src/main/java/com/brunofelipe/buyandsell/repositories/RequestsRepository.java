package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunofelipe.buyandsell.entity.Requests;

@Repository
public interface RequestsRepository extends JpaRepository <Requests, Integer> {

}
