package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.brunofelipe.buyandsell.entity.Categorys;

@Repository
public interface CategorysRepository extends JpaRepository <Categorys, Integer> {

}
