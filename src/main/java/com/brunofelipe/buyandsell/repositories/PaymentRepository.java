package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunofelipe.buyandsell.entity.Payment;

@Repository
public interface PaymentRepository extends JpaRepository <Payment, Integer> {

}
