package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunofelipe.buyandsell.entity.States;

@Repository
public interface StatesRepository extends JpaRepository <States, Integer> {

}
