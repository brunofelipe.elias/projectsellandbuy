package com.brunofelipe.buyandsell.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunofelipe.buyandsell.entity.City;

@Repository
public interface CityRepository extends JpaRepository <City, Integer> {

}
