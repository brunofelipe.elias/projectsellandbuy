package com.brunofelipe.buyandsell.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brunofelipe.buyandsell.entity.Client;
import com.brunofelipe.buyandsell.services.ClientService;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@RestController
@RequestMapping(value = "/cliente")
public class ClientResource {
	
	@Autowired
	private ClientService clientService;
	
	@RequestMapping(value = "/{idClient}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer idClient) throws ObjectNotFoundException {
		Client obj = clientService.search(idClient);
		return ResponseEntity.ok().body(obj);
	}
}
