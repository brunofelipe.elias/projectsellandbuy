package com.brunofelipe.buyandsell.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.brunofelipe.buyandsell.dto.CategorysDTO;
import com.brunofelipe.buyandsell.entity.Categorys;
import com.brunofelipe.buyandsell.services.CategorysService;


@RestController
@RequestMapping(value = "/categorias")
public class CategorysResource {
	
	@Autowired
	private CategorysService categorysService;
	
	@RequestMapping(value = "/{idCategorys}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer idCategorys){
		Categorys obj = categorysService.search(idCategorys);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody Categorys category){
		category = categorysService.insert(category);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{idCategorys}").buildAndExpand(category.getIdCategorys()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/{idCategorys}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody Categorys category,@PathVariable Integer idCategorys){
		category.setIdCategorys(idCategorys);
		category = categorysService.update(category);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/{idCategorys}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer idCategorys){
		categorysService.delete(idCategorys);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CategorysDTO>> findAll(){
		List<Categorys> list = categorysService.findAll();
		List<CategorysDTO> listDto = list.stream().map(categorys -> new CategorysDTO(categorys)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
}
