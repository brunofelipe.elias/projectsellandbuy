package com.brunofelipe.buyandsell.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brunofelipe.buyandsell.entity.Requests;
import com.brunofelipe.buyandsell.services.RequestsService;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@RestController
@RequestMapping(value = "/pedidos")
public class RequestsResource {
	
	@Autowired
	private RequestsService requestsService;
	
	@RequestMapping(value = "/{idRequests}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer idRequests) throws ObjectNotFoundException {
		Requests obj = requestsService.search(idRequests);
		return ResponseEntity.ok().body(obj);
	}
}
