package com.brunofelipe.buyandsell.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brunofelipe.buyandsell.entity.Products;
import com.brunofelipe.buyandsell.services.ProductsService;
import com.brunofelipe.buyandsell.services.exception.ObjectNotFoundException;


@RestController
@RequestMapping(value = "/produtos")
public class ProductsResource {
	
	@Autowired
	private ProductsService productsService;
	
	@RequestMapping(value = "/{idProducts}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer idProducts) throws ObjectNotFoundException {
		Products obj = productsService.search(idProducts);
		return ResponseEntity.ok().body(obj);
		
	}
}
