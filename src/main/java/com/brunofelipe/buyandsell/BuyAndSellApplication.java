package com.brunofelipe.buyandsell;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.brunofelipe.buyandsell.entity.Address;
import com.brunofelipe.buyandsell.entity.BillPayment;
import com.brunofelipe.buyandsell.entity.CardPayment;
import com.brunofelipe.buyandsell.entity.Categorys;
import com.brunofelipe.buyandsell.entity.City;
import com.brunofelipe.buyandsell.entity.Client;
import com.brunofelipe.buyandsell.entity.OrderedItem;
import com.brunofelipe.buyandsell.entity.Payment;
import com.brunofelipe.buyandsell.entity.Products;
import com.brunofelipe.buyandsell.entity.Requests;
import com.brunofelipe.buyandsell.entity.States;
import com.brunofelipe.buyandsell.entity.enums.StatusPayment;
import com.brunofelipe.buyandsell.entity.enums.TypeClient;
import com.brunofelipe.buyandsell.repositories.AddressRepository;
import com.brunofelipe.buyandsell.repositories.CategorysRepository;
import com.brunofelipe.buyandsell.repositories.CityRepository;
import com.brunofelipe.buyandsell.repositories.ClientRepository;
import com.brunofelipe.buyandsell.repositories.OrderedItemRepository;
import com.brunofelipe.buyandsell.repositories.PaymentRepository;
import com.brunofelipe.buyandsell.repositories.ProductsRepository;
import com.brunofelipe.buyandsell.repositories.RequestsRepository;
import com.brunofelipe.buyandsell.repositories.StatesRepository;

@SpringBootApplication
public class BuyAndSellApplication implements CommandLineRunner{

	@Autowired
	private CategorysRepository categorysRepository;
	@Autowired
	private ProductsRepository productsRepository;
	@Autowired
	private StatesRepository statesRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private RequestsRepository requestsRepository;
	@Autowired
	private PaymentRepository paymentRepository;
	@Autowired
	private OrderedItemRepository orderedItemRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(BuyAndSellApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception{
		
		Categorys fastFood = new Categorys(null,"Alimentação Rápida");
		Categorys coffeeBreak = new Categorys(null, "Alimentação para Festas de Empresa");
		
		Products chips = new Products(null, "Batata Frita", 15.00);
		Products pie = new Products(null, "Torta de Limão", 70.05);
		Products hamburger = new Products(null, "Hambúrge Simples", 25.65);

		fastFood.getProducts().addAll(Arrays.asList(chips,hamburger));
		coffeeBreak.getProducts().addAll(Arrays.asList(pie,hamburger));
		
		chips.getCategorys().addAll(Arrays.asList(fastFood));
		pie.getCategorys().addAll(Arrays.asList(coffeeBreak));
		hamburger.getCategorys().addAll(Arrays.asList(coffeeBreak,fastFood));

		categorysRepository.saveAll(Arrays.asList(fastFood,coffeeBreak));
		productsRepository.saveAll(Arrays.asList(chips,pie,hamburger));
		
		States pernambuco = new States(null, "Pernambuco");
		States sp = new States(null, "São Paulo");

		City cabo = new City(null, "Cabo de Santo Agostinho", pernambuco);
		City jaboatao = new City(null, "Jaboatão dos Guararapes", pernambuco);
		City oli = new City(null, "Olinda", pernambuco);
		City bv = new City(null, "Boa Viagem", pernambuco);
		
		City saoPaulo = new City(null,"São Paulo",sp);
		City urbe = new City(null,"Urbelândia",sp);
		City arara = new City(null,"Araraquara",sp);
		
		pernambuco.getCitys().addAll(Arrays.asList(cabo,jaboatao,oli,bv));
		sp.getCitys().addAll(Arrays.asList(saoPaulo,arara,urbe));
		
		statesRepository.saveAll(Arrays.asList(pernambuco,sp));
		cityRepository.saveAll(Arrays.asList(cabo,jaboatao,oli,bv,saoPaulo,urbe,arara));

		Client bruno = new Client(null, "Bruno Felipe Elias Santos", "brunofelipe.elias@gmail.com",
							"101.660.984-18", TypeClient.PHYSICAL_PERSON);
		bruno.getPhone().addAll(Arrays.asList("9885-5178", "9934-9042"));

		Client janaina = new Client(null, "Janaína Maiara Leandro de Melo", "mjanamelo@gmail.com",
							"101.660.984-17", TypeClient.PHYSICAL_PERSON);
		janaina.getPhone().addAll(Arrays.asList("9934-9041", "9885-5177"));
		
		Address boa = new Address(null, "R. José Domingues da Silva", "5A", 
				"Edifício", "116 - Boa Viagem", "51030-070", janaina, bv);
		
		Address csa = new Address(null, "R. José Lins Teles", "12", 
				"Residencia", "Vila Social", "54510-280", bruno, cabo);
		
		bruno.getAddress().addAll(Arrays.asList(csa,boa));
		janaina.getAddress().addAll(Arrays.asList(csa,boa));
		
		clientRepository.saveAll(Arrays.asList(bruno,janaina));
		addressRepository.saveAll(Arrays.asList(boa,csa));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		Requests firstCommand = new Requests(null,sdf.parse("07/11/2019 10:25"),bruno,csa);
		Requests secondCommand = new Requests(null,sdf.parse("07/11/2019 10:25"),bruno,csa);
		Requests threeCommand = new Requests(null,sdf.parse("07/11/2019 10:25"),bruno,csa);
		
		Payment paymentCard = new CardPayment(null, StatusPayment.SETTLED, firstCommand, 3);
		firstCommand.setPayment(paymentCard);
		
		Payment paymentBill = new BillPayment(null, StatusPayment.PENDING, secondCommand,
				null,sdf.parse("08/11/2019 15:15"));
		secondCommand.setPayment(paymentBill);
		
		Payment paymentCancel = new BillPayment(null, StatusPayment.CANCELED, threeCommand,
				null,sdf.parse("07/11/2019 15:15"));
		threeCommand.setPayment(paymentCancel);
		
		bruno.getRequests().addAll(Arrays.asList(firstCommand,secondCommand,threeCommand));
		
		requestsRepository.saveAll(Arrays.asList(firstCommand,secondCommand,threeCommand));
		paymentRepository.saveAll(Arrays.asList(paymentCard,paymentBill,paymentCancel));
		
		OrderedItem firstOrderedItem = new OrderedItem(firstCommand,hamburger,5.0,5,2580.50);
		OrderedItem secondOrderedItem = new OrderedItem(secondCommand,pie,5.0,5,2580.50);
		OrderedItem threeOrderedItem = new OrderedItem(threeCommand,chips,5.0,40,8592.09);
		
		firstCommand.getItems().addAll(Arrays.asList(firstOrderedItem,threeOrderedItem));
		secondCommand.getItems().addAll(Arrays.asList(secondOrderedItem));
		threeCommand.getItems().addAll(Arrays.asList(firstOrderedItem,secondOrderedItem,threeOrderedItem));
		
		hamburger.getItems().addAll(Arrays.asList(firstOrderedItem));
		pie.getItems().addAll(Arrays.asList(secondOrderedItem));
		chips.getItems().addAll(Arrays.asList(threeOrderedItem));
		
		orderedItemRepository.saveAll(Arrays.asList(firstOrderedItem,secondOrderedItem,threeOrderedItem));
		
	}
}
