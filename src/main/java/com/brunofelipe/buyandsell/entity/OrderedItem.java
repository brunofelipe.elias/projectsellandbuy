package com.brunofelipe.buyandsell.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class OrderedItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@EmbeddedId  
	private OrderedItemPK idOrderedItem = new OrderedItemPK();
	
	private Double discount;
	private Integer amount;
	private Double price;
	//private Double subTotal = (amount * price)/discountCal; fazendo calculo.
	//private Double discountCal = (discount / 100);
	public OrderedItem() {
	}

	public OrderedItem(Requests requests, Products products , Double discount, Integer amount, Double price) {
		super();
		idOrderedItem.setProducts(products);
		idOrderedItem.setRequests(requests);
		this.discount = discount;
		this.amount = amount;
		this.price = price;
	}
	
	public Products getProducts() {
		return idOrderedItem.getProducts();
	}

	@JsonIgnore
	public Requests getRequests() {
		return idOrderedItem.getRequests();
	}

	public OrderedItemPK getIdOrderedItem() {
		return idOrderedItem;
	}

	public void setIdOrderedItem(OrderedItemPK idOrderedItem) {
		this.idOrderedItem = idOrderedItem;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idOrderedItem == null) ? 0 : idOrderedItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderedItem other = (OrderedItem) obj;
		if (idOrderedItem == null) {
			if (other.idOrderedItem != null)
				return false;
		} else if (!idOrderedItem.equals(other.idOrderedItem))
			return false;
		return true;
	}
	
}
