package com.brunofelipe.buyandsell.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Categorys implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idCategorys;
	private String name;

	@ManyToMany(mappedBy = "categorys")
	private List<Products> products = new ArrayList<>();
	
	public Categorys() {
	}

	public Categorys(Integer idCategorys, String name) {
		super();
		this.idCategorys = idCategorys;
		this.name = name;
	}

	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}

	public Integer getIdCategorys() {
		return idCategorys;
	}

	public void setIdCategorys(Integer idCategorys) {
		this.idCategorys = idCategorys;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCategorys == null) ? 0 : idCategorys.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorys other = (Categorys) obj;
		if (idCategorys == null) {
			if (other.idCategorys != null)
				return false;
		} else if (!idCategorys.equals(other.idCategorys))
			return false;
		return true;
	}

}
