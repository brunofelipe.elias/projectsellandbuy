package com.brunofelipe.buyandsell.entity.enums;

public enum StatusPayment {
	
	PENDING(30, " Pendente. "),
	SETTLED(40, " Quitado. "),
	CANCELED(50, " Cancelado. ");
	
	
	private int cod;
	private String description;
	
	private StatusPayment(int cod, String description) {
		this.cod = cod;
		this.description = description;
	}

	public int getCod() {
		return cod;
	}

	public String getDescription() {
		return description;
	}
	
	public static StatusPayment toEnum(Integer cod) {
		
		if (cod == null) {
			return null;
		}
		
		for (StatusPayment sp : StatusPayment.values()) {
			if (cod.equals(sp.getCod())) {
				return sp ;
			}
			
		}
		throw new IllegalArgumentException("Não é valido a situação, especifique ou entre em contato com nossa equipe."+ cod);
	}
	
}
