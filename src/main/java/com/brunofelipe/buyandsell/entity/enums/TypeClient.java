package com.brunofelipe.buyandsell.entity.enums;

public enum TypeClient {
	
	PHYSICAL_PERSON(10, " Pessoa Física. "),
	LEGAL_PERSON(20, " Pessoa Jurídica. ");
	
	
	private int cod;
	private String description;
	
	private TypeClient(int cod, String description) {
		this.cod = cod;
		this.description = description;
	}

	public int getCod() {
		return cod;
	}

	public String getDescription() {
		return description;
	}
	
	public static TypeClient toEnum(Integer cod) {
		
		if (cod == null) {
			return null;
		}
		
		for (TypeClient tc : TypeClient.values()) {
			if (cod.equals(tc.getCod())) {
				return tc;
			}
			
		}
		throw new IllegalArgumentException("Não é valido a Pessoa, especifique ou entre em contato com nossa equipe."+ cod);
	}
	
}
