package com.brunofelipe.buyandsell.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import com.brunofelipe.buyandsell.entity.enums.StatusPayment;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Payment implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idPayment;
	private Integer statusPayment;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "requests_id")
	@MapsId
	private Requests requests;
	
	public Payment() {
	}

	public Payment(Integer idPayment, StatusPayment statusPayment, Requests requests) {
		super();
		this.idPayment = idPayment;
		this.statusPayment = statusPayment.getCod();
		this.requests = requests;
	}

	public Integer getIdPayment() {
		return idPayment;
	}

	public void setIdPayment(Integer idPayment) {
		this.idPayment = idPayment;
	}

	public StatusPayment getStatusPayment() {
		return StatusPayment.toEnum(statusPayment);
	}

	public void setStatusPayment(StatusPayment statusPayment) {
		this.statusPayment = statusPayment.getCod();
	}

	public Requests getRequests() {
		return requests;
	}

	public void setRequests(Requests requests) {
		this.requests = requests;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPayment == null) ? 0 : idPayment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (idPayment == null) {
			if (other.idPayment != null)
				return false;
		} else if (!idPayment.equals(other.idPayment))
			return false;
		return true;
	}

}