package com.brunofelipe.buyandsell.entity;

import java.util.Date;

import javax.persistence.Entity;

import com.brunofelipe.buyandsell.entity.enums.StatusPayment;
import com.fasterxml.jackson.annotation.JsonFormat;
 
@Entity
public class BillPayment extends Payment {
	private static final long serialVersionUID = 1L;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date paymentCompleted;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date paymentExpired;
	
	public BillPayment() {
	}

	public BillPayment(Integer idPayment, StatusPayment statusPayment, Requests requests,
			Date paymentCompleted,Date paymentExpired) {
		super(idPayment, statusPayment, requests);
		this.paymentCompleted = paymentCompleted;
		this.paymentExpired = paymentExpired;
	}

	public Date getPaymentCompleted() {
		return paymentCompleted;
	}

	public void setPaymentCompleted(Date paymentCompleted) {
		this.paymentCompleted = paymentCompleted;
	}

	public Date getPaymentExpired() {
		return paymentExpired;
	}

	public void setPaymentExpired(Date paymentExpired) {
		this.paymentExpired = paymentExpired;
	}

	
}
