package com.brunofelipe.buyandsell.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Requests implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idRequests;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date instant;
	
	@OneToOne(cascade = CascadeType.ALL,mappedBy = "requests")
	private Payment payment;
	
	@ManyToOne
	@JoinColumn(name = "client_id")
	private Client client;
	
	@ManyToOne 
	@JoinColumn(name = "deliveryAddress_id")
	private Address deliveryAddress;
	
	@OneToMany(mappedBy = "idOrderedItem.requests")
	private Set<OrderedItem> items = new HashSet<>();
	
	public Requests() {
	}

	public Requests(Integer idRequests, Date instant, Client client, Address deliveryAddress) {
		super();
		this.idRequests = idRequests;
		this.instant = instant;
		this.client = client;
		this.deliveryAddress = deliveryAddress;
	}

	public Integer getIdRequests() {
		return idRequests;
	}

	public void setIdRequests(Integer idRequests) {
		this.idRequests = idRequests;
	}

	public Date getInstant() {
		return instant;
	}

	public void setInstant(Date instant) {
		this.instant = instant;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	public Set<OrderedItem> getItems() {
		return items;
	}

	public void setItems(Set<OrderedItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRequests == null) ? 0 : idRequests.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Requests other = (Requests) obj;
		if (idRequests == null) {
			if (other.idRequests != null)
				return false;
		} else if (!idRequests.equals(other.idRequests))
			return false;
		return true;
	}

}