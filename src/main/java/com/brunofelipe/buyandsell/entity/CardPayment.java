package com.brunofelipe.buyandsell.entity;

import javax.persistence.Entity;

import com.brunofelipe.buyandsell.entity.enums.StatusPayment;

@Entity
public class CardPayment extends Payment {
	private static final long serialVersionUID = 1L;

	private Integer numberOfInstallments;

	public CardPayment() {
	}

	public CardPayment(Integer idPayment, StatusPayment statusPayment,
			Requests requests, Integer numberOfInstallments) {
		super(idPayment, statusPayment, requests);
		
		this.numberOfInstallments = numberOfInstallments;
	}

	public Integer getNumberOfInstallments() {
		return numberOfInstallments;
	}

	public void setNumberOfInstallments(Integer numberOfInstallments) {
		this.numberOfInstallments = numberOfInstallments;
	}
	
	
}
