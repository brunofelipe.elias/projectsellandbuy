package com.brunofelipe.buyandsell.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Products implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idProduct;
	private String name;
	private Double price;

	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "Products_Categorys",
		joinColumns = @JoinColumn(name = "id_products"),
		inverseJoinColumns = @JoinColumn(name = "id_categorys"))
	private List<Categorys> categorys = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "idOrderedItem.products")
	private Set<OrderedItem> items = new HashSet<>();
	
	public Products() {
	}

	public Products(Integer idProduct, String name, Double price) {
		super();
		this.idProduct = idProduct;
		this.name = name;
		this.price = price;
	}

	@JsonIgnore
	public List<Requests> getRequests(){
		List<Requests> list = new ArrayList<>(); 
		for (OrderedItem iP : items) {
			list.add(iP.getRequests());
		}
		return list;
	}
	
	public List<Categorys> getCategorys() {
		return categorys;
	}

	public void setCategorys(List<Categorys> categorys) {
		this.categorys = categorys;
	}
	
	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Set<OrderedItem> getItems() {
		return items;
	}

	public void setItems(Set<OrderedItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idProduct == null) ? 0 : idProduct.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Products other = (Products) obj;
		if (idProduct == null) {
			if (other.idProduct != null)
				return false;
		} else if (!idProduct.equals(other.idProduct))
			return false;
		return true;
	}

}