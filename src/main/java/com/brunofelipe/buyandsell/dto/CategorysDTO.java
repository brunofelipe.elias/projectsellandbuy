package com.brunofelipe.buyandsell.dto;

import java.io.Serializable;

import com.brunofelipe.buyandsell.entity.Categorys;

public class CategorysDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer idCategorys;
	private String name;
	
	public CategorysDTO () {
		
	}
	
	public CategorysDTO(Categorys categorys) {
		idCategorys = categorys.getIdCategorys();
		name = categorys.getName();
	}

	public Integer getIdCategorys() {
		return idCategorys;
	}

	public void setIdCategorys(Integer idCategorys) {
		this.idCategorys = idCategorys;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
